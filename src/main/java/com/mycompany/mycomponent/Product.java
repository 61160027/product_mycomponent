/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mycomponent;

import java.util.ArrayList;

/**
 *
 * @author PC
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String image;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }

    public static ArrayList<Product> genProductList() {
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1, "latte 1", 40, "1.jpg"));
        list.add(new Product(2, "latte 2", 40, "1.jpg"));
        list.add(new Product(3, "latte 3", 40, "1.jpg"));
        list.add(new Product(4, "mocca 4", 40, "1.jpg"));
        list.add(new Product(5, "mocca 5", 40, "1.jpg"));
        list.add(new Product(6, "mocca 6", 40, "1.jpg"));
        return  list;
    }

}
